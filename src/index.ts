import Bus from './nes/NES';
import MOS6502 from './nes/MOS6502';
import Debugger from './Debugger';
import Visual2C02 from './nes/Visual2C02';
import Cartridge from './nes/Cartridge';

const cpu = new MOS6502();
const ppu = new Visual2C02();
const nes = new Bus(cpu, ppu);
const deb = new Debugger(nes);

let cartridge;

const getCartridgeData = (e: Event) => {
  const file: File = (e.target as HTMLInputElement).files[0];
  const reader: FileReader = new FileReader();

  reader.addEventListener('load', () => {
    const buffer: Buffer = Buffer.from(reader.result);
    cartridge = new Cartridge(buffer);
    nes.insertCartridge(cartridge);

    nes.reset();

    deb.disassamble();
    deb.draw();
  });

  reader.readAsArrayBuffer(file);
};

const handleButtonClick = async () => {
  const res: Response = await fetch('./roms/nestest.nes');
  const arrayBuffer: ArrayBuffer = await res.arrayBuffer();
  const arrayBufferView: Uint8Array = new Uint8Array(arrayBuffer);

  const buffer: Buffer = Buffer.from(arrayBufferView);

  cartridge = new Cartridge(buffer);
  nes.insertCartridge(cartridge);

  nes.reset();

  // Testing
  cpu.programCounter = 0xC000;
  cpu.testingEnabled = true;

  deb.disassamble();
  deb.draw();
};

const $cartridgeInput = document.getElementById('cartridge');
$cartridgeInput.addEventListener('change', getCartridgeData);

const $cartridgeButton = document.getElementById('defaultRom');
$cartridgeButton.addEventListener('click', handleButtonClick);
