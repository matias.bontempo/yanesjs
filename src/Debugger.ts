import Bus from './nes/NES';
import { uint16, uint8 } from './nes/types';
import { Flags } from './nes/enums';
import { DisassembledInstruction } from './nes/interfaces';

const breakpoints = [0xc825, 0xC6BC, 0xDE2A, 0xE517];

const ySpacing = 13;

class Debugger {
  el: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;

  width: number = 900;
  height: number = 600;

  nes: Bus

  frameBuffer: Array<Uint8Array> = new Array(256);
  patternTableBuffer: Uint8Array[] = [new Uint8Array(128 * 128), new Uint8Array(128 * 128)];

  elPatternTable: HTMLCanvasElement;
  ctxPatternTable: CanvasRenderingContext2D;

  mapAsm: DisassembledInstruction[];

  emulationRun: boolean = false;

  selectedPalette: uint8 = 0x00;

  fontSize: number = 12;

  constructor(nes: Bus) {
    this.nes = nes;
    console.log({ nes });

    this.initMainDebugCanvas();
    this.initPatternTableCanvas();

    document.addEventListener('keydown', this.handleKeyDown.bind(this));

    // Initialize frame buffer
    for (let i = 0; i < this.width; i += 1) {
      this.frameBuffer[i] = new Uint8Array(this.height);
      for (let j = 0; j < this.height; j += 1) {
        this.frameBuffer[i][j] = 0xFF;
      }
    }

    this.run();
  }

  initMainDebugCanvas(): void {
    const scale = window.devicePixelRatio;

    this.el = document.createElement('canvas');
    this.el.width = this.width * scale;
    this.el.height = this.height * scale;

    this.el.style.width = `${this.width}px`;
    this.el.style.height = `${this.height}px`;
    this.el.style.imageRendering = 'pixelated';
    this.el.style.backgroundColor = 'black';

    this.ctx = this.el.getContext('2d');
    this.ctx.imageSmoothingEnabled = false;
    this.ctx.scale(scale, scale);

    document.body.append(this.el);
  }

  initPatternTableCanvas(): void {
    const width: number = 256;
    const height: number = 128;

    this.elPatternTable = document.createElement('canvas');
    this.elPatternTable.width = width;
    this.elPatternTable.height = height;

    this.ctxPatternTable = this.elPatternTable.getContext('2d');
    this.ctxPatternTable.imageSmoothingEnabled = false;

    this.ctxPatternTable.fillStyle = 'black';
    this.ctxPatternTable.fillRect(0, 0, width, height);
  }

  handleKeyDown(e: KeyboardEvent): void {
    const { code } = e;
    if (code === 'Space') {
      this.emulationRun = !this.emulationRun;
      this.draw();
    } else if (code === 'KeyP') {
      this.selectedPalette += 1;
      this.selectedPalette &= 0x07;
      this.draw();
    } else if (code === 'KeyX') {
      while (breakpoints.indexOf(this.nes.cpu.programCounter) === -1) {
        this.nes.clock();
      }
      this.draw();
    } else if (code === 'KeyC') {
      do {
        this.nes.clock();
      } while (this.nes.cpu.cycles > 0);
      this.nes.clock();
      this.nes.clock();
      this.draw();
    } else if (code === 'KeyV') {
      for (let i = 0; i < 128; i += 1) {
        do {
          this.nes.clock();
        } while (this.nes.cpu.cycles > 0);
        this.nes.clock();
        this.nes.clock();
      }
      this.draw();
    } else if (code === 'KeyL') {
      do {
        this.nes.clock();
      } while (!this.nes.ppu.lineComplete);
      this.draw();
      this.nes.ppu.lineComplete = false;
    } else if (code === 'KeyF') {
      do {
        this.nes.clock();
      } while (!this.nes.ppu.frameComplete);
      this.draw();
      this.nes.ppu.frameComplete = false;
    } else if (code === 'KeyR') {
      this.nes.reset();
      this.draw();
    } else if (code === 'KeyI') {
      this.nes.cpu.irq();
      this.draw();
    } else if (code === 'KeyN') {
      this.nes.cpu.nmi();
      this.draw();
    }
  }

  disassamble(): void {
    this.mapAsm = this.nes.cpu.disassamble(0x0000, 0xFFFF);
  }

  draw(): void {
    this.ctx.font = `${this.fontSize}px Cousine, monoSpacing`;
    this.ctx.textBaseline = 'top';

    // this.drawNamesTable(0, 0);
    this.drawScreen(0, 0);

    this.drawPalettes(0, 256 + ySpacing * 1);
    this.drawTable(0, 256 + ySpacing * 3, 0);
    this.drawTable(128, 256 + ySpacing * 3, 1);

    this.drawCPU(270, ySpacing * 21);
    this.drawCode(8, 270, ySpacing * 29);

    this.drawPPU(270, ySpacing * 40);

    this.drawMemoryPage(0x0000, 480, ySpacing * 21);

    // this.drawInstructions(0, 468);
  }

  drawMemoryPage(from: uint16, x: number, y: number): void {
    const xOffset = 52;
    const xSpacing = 20;

    this.ctx.clearRect(x, y, x + xOffset + 16 * xSpacing, y + 16 * ySpacing);

    this.ctx.fillStyle = 'white';

    for (let i = 0; i < 16; i += 1) {
      const rowAddress = from + (16 * i);
      const row = `#${rowAddress.toString(16).padStart(4, '0000')}:`;

      this.ctx.fillText(row, x, y + (i * ySpacing));

      for (let j = 0; j < 16; j += 1) {
        const address = rowAddress + j;
        const data = this.nes.readBusCPU(address).toString(16).padStart(2, '00');

        this.ctx.fillText(data, x + xOffset + (j * xSpacing), y + (i * ySpacing));
      }
    }
  }

  drawCPU(x: number, y: number): void {
    const drawBit = (flag: Flags, label: string): void => {
      const spacing = 64;

      const offset = (7 - (Math.log(flag) / Math.log(2))) * 12;

      this.ctx.fillStyle = 'green';
      if ((this.nes.cpu.status & flag) === 0) this.ctx.fillStyle = 'red';
      this.ctx.fillText(label, x + spacing + offset, y);
    };

    const drawStatus = () => {
      this.ctx.fillStyle = 'white';
      this.ctx.fillText('STATUS:', x, y);

      drawBit(Flags.N, 'N');
      drawBit(Flags.V, 'V');
      drawBit(Flags.U, '-');
      drawBit(Flags.B, 'B');
      drawBit(Flags.D, 'D');
      drawBit(Flags.I, 'I');
      drawBit(Flags.Z, 'Z');
      drawBit(Flags.C, 'C');

      this.ctx.fillStyle = 'white';
      this.ctx.fillText((this.nes.cpu.status & ~Flags.U).toString(10), x + 165, y);
    };

    const drawTextLine = (text, position) => {
      this.ctx.fillStyle = 'white';
      this.ctx.fillText(text, x, y + (position * ySpacing));
    };

    const drawValue = (label, value, position) => {
      drawTextLine(`${label}: ${value}`, position);
    };

    const drawPointer = (label, value, position) => {
      drawTextLine(`${label}: $${value.toString(16).padStart(4, '0000')}`, position);
    };

    const drawRegister = (label, value, position) => {
      drawTextLine(`${label}: $${value.toString(16).padStart(2, '00')} [${value.toString(10)}]`, position);
    };

    this.ctx.clearRect(x, y, 180, ySpacing * 6);
    drawStatus();
    drawPointer('PC', this.nes.cpu.programCounter, 1);
    drawRegister('A', this.nes.cpu.a, 2);
    drawRegister('X', this.nes.cpu.x, 3);
    drawRegister('Y', this.nes.cpu.y, 4);
    drawPointer('Stack Pointer', 0x0100 + this.nes.cpu.stackPointer, 5);
    drawValue('Cycles', this.nes.cpu.elapsedCycles, 6);
  }

  drawCode(qty: number, x: number, y: number): void {
    const { programCounter } = this.nes.cpu;
    const instructionStart = this.mapAsm.find((i) => i.address === programCounter);
    const idxStart = this.mapAsm.indexOf(instructionStart);

    if (idxStart === -1) return;

    const prevInstructions = 2;

    this.ctx.clearRect(x, y, 176, (prevInstructions + qty) * ySpacing);

    this.ctx.fillStyle = 'blue';
    this.ctx.fillRect(x, y + ySpacing * prevInstructions - 1, 176, ySpacing);

    this.ctx.fillStyle = 'white';

    try {
      for (let i = idxStart - prevInstructions; i < idxStart; i += 1) {
        const instruction = this.mapAsm[i];
        if (!instruction) return;
        const line = `$${instruction.address.toString(16).padStart(4, '0000')}: ${instruction.operation} ${instruction.data}`;
        this.ctx.fillText(line, x, y + ySpacing * (i - idxStart + prevInstructions));
      }

      for (let i = idxStart; i < idxStart + qty; i += 1) {
        const instruction = this.mapAsm[i];
        const line = `$${instruction.address.toString(16).padStart(4, '0000')}: ${instruction.operation} ${instruction.data}`;
        this.ctx.fillText(line, x, y + ySpacing * (i - idxStart + prevInstructions));
      }
    } catch (err) {
      console.error(err);
    }
  }

  drawInstructions(x: number, y: number) {
    this.ctx.fillStyle = 'white';
    this.ctx.fillText(`SPACE = Emulation Time [${this.emulationRun ? 'X' : ' '}]    C = Next Cycle    F = Next Frame    R = Reset    I = IRQ    N = NMI`, x, y);
  }

  drawScreen(x: number, y: number) {
    this.ctx.clearRect(0, 0, 256, 240);
    const t = performance.now();
    const frame: Array<Uint8Array> = this.nes.ppu.getScreen();
    const width = 256;
    const height = 240;
    for (let i = 0; i < width; i += 1) {
      for (let j = 0; j < height; j += 1) {
        if (frame[i][j] !== this.frameBuffer[i][j]) {
          this.frameBuffer[i][j] = frame[i][j];
          this.ctx.fillStyle = this.nes.ppu.palettes[frame[i][j]];
          this.ctx.fillRect(x + i, y + j, 1, 1);
        }
      }
    }
    this.ctx.fillStyle = 'white';
    this.ctx.fillText(`${Math.floor((performance.now() - t))}ms`, x, y + height + 8);
  }

  drawPalettes(x: number, y: number) {
    this.ctx.clearRect(x, y, 256, 16);

    for (let p = 0; p < 8; p += 1) {
      for (let c = 0; c < 4; c += 1) {
        this.ctx.fillStyle = this.nes.ppu.palettes[this.nes.ppu.getColorFromPaletteRam(p as uint8, c as uint8)];
        this.ctx.fillRect(x + (p * (256 / 8)) + (c * 8), y, 256 / 32, 16);
      }
      this.ctx.strokeStyle = this.selectedPalette === p ? 'white' : 'gray';
      this.ctx.strokeRect(x + (p * (256 / 8)), y, 256 / 8, 16);
    }
  }

  drawTable(x: number, y: number, tableId: uint8) {
    const width = 128;
    const height = 128;

    const table = this.nes.ppu.getPatternTable(tableId, this.selectedPalette);

    for (let i = 0; i < width; i += 1) {
      for (let j = 0; j < height; j += 1) {
        const index = (i * width) + j;
        if (table[index] !== this.patternTableBuffer[tableId][index]) {
          this.patternTableBuffer[tableId][index] = table[tableId][index];
          this.ctxPatternTable.fillStyle = this.nes.ppu.palettes[table[index]];
          this.ctxPatternTable.fillRect((width * tableId) + i, j, 1, 1);
        }
      }
    }

    this.ctx.drawImage(this.elPatternTable, width * tableId, 0, width, height, x, y, width, height);
  }

  drawNamesTable(x: number, y: number) {
    this.ctx.clearRect(x, y, 256, 256);
    this.ctx.clearRect(x + 264, y, 32 * 18, 32 * 14);

    this.ctx.fillStyle = 'white';
    this.ctx.font = '8px Cousine, monoSpacing';

    for (let i = 0; i < 32; i += 1) {
      for (let j = 0; j < 32; j += 1) {
        const idx = (i * 32) + j;
        const objectId = this.nes.ppu.tblName[0][idx];
        this.ctx.fillText(objectId.toString(16).padStart(2, '00'), x + 264 + (j * 10), y + (i * 8));
        this.ctx.drawImage(this.elPatternTable, 8 * (objectId & 0x0F), 8 * (objectId >> 4), 8, 8, x + (j * 8), y + (i * 8), 8, 8);
      }
    }

    this.ctx.fillStyle = 'white';
    this.ctx.font = `${this.fontSize}px Cousine, monoSpacing`;
  }

  drawPPU(x: number, y: number) {
    this.ctx.clearRect(x, y, 240, ySpacing * 3);
    this.ctx.fillStyle = 'white';
    this.ctx.fillText(`PPU Address: ${this.nes.ppu.addressLatch.toString(16).padStart(4, '0000')}`, x, y);
    this.ctx.fillText(`PPU Scanline: ${this.nes.ppu.scanline}`, x, y + ySpacing * 1);
    this.ctx.fillText(`PPU Pixel: ${this.nes.ppu.cycle}`, x, y + ySpacing * 2);
  }

  run() {
    const loop = () => {
      if (this.emulationRun) {
        do { this.nes.clock(); } while (!this.nes.ppu.frameComplete);
        this.nes.ppu.frameComplete = false;
        this.draw();
      }
      requestAnimationFrame(loop);
    };
    requestAnimationFrame(loop);
  }
}

export default Debugger;
