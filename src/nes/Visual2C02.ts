import { uint8, uint16 } from './types';
import Cartridge from './Cartridge';
import { LoopyRegister, Pointer } from './interfaces';
import {
  StatusPPU, ControlPPU, Mirroring, MaskPPU,
} from './enums';

class Visual2C02 {
  cartridge: Cartridge;

  lineComplete: boolean = false;
  frameComplete: boolean = false;

  vRamTableName: Uint8Array[] = [new Uint8Array(1024), new Uint8Array(1024)];
  ramTablePalette: Uint8Array = new Uint8Array(32);

  tblName: Uint8Array[] = [new Uint8Array(32 * 32 * 2), new Uint8Array(32 * 32 * 2)];
  tblPattern: Uint8Array[] = [new Uint8Array(4096), new Uint8Array(4096)];
  tblPalette: Uint8Array = new Uint8Array(32);

  controlRegister: uint8 = 0x00;
  maskRegister: uint8 = 0x00;
  statusRegister: uint8 = 0x00;

  nmi: boolean = false;

  addressLatch: uint8 = 0x00;
  ppuDataBuffer: uint8 = 0x00;
  ppuAddress: uint16 = 0x0000;

  vRamAddress: LoopyRegister;
  tRamAddress: LoopyRegister;
  fineX: uint8 = 0x00;

  bgNextTileId: uint8 = 0x00;
  bgNextTileAttribute: uint8 = 0x00;
  bgNextTileLsb: uint8 = 0x00;
  bgNextTileMsb: uint8 = 0x00;

  bgShifterPatternLo: uint16 = 0x0000;
  bgShifterPatternHi: uint16 = 0x0000;
  bgShifterAttributeLo: uint16 = 0x0000;
  bgShifterAttributeHi: uint16 = 0x0000;

  scanline: number = 0;
  cycle: number = 0;

  private width: number = 256;
  private height: number = 240;

  private frame: Array<Uint8Array> = new Array(this.width);

  palettes: Array<string> = new Array(0x40);
  private sprNameTable: Uint8Array[] = [new Uint8Array(128 * 128), new Uint8Array(128 * 128)];
  private sprPatternTable: Uint8Array[] = [new Uint8Array(128 * 128), new Uint8Array(128 * 128)];

  constructor() {
    // Initialize palettes
    this.palettes[0x00] = 'rgb(84, 84, 84)';
    this.palettes[0x01] = 'rgb(0, 30, 116)';
    this.palettes[0x02] = 'rgb(8, 16, 144)';
    this.palettes[0x03] = 'rgb(48, 0, 136)';
    this.palettes[0x04] = 'rgb(68, 0, 100)';
    this.palettes[0x05] = 'rgb(92, 0, 48)';
    this.palettes[0x06] = 'rgb(84, 4, 0)';
    this.palettes[0x07] = 'rgb(60, 24, 0)';
    this.palettes[0x08] = 'rgb(32, 42, 0)';
    this.palettes[0x09] = 'rgb(8, 58, 0)';
    this.palettes[0x0A] = 'rgb(0, 64, 0)';
    this.palettes[0x0B] = 'rgb(0, 60, 0)';
    this.palettes[0x0C] = 'rgb(0, 50, 60)';
    this.palettes[0x0D] = 'rgb(0, 0, 0)';
    this.palettes[0x0E] = 'rgb(0, 0, 0)';
    this.palettes[0x0F] = 'rgb(0, 0, 0)';

    this.palettes[0x10] = 'rgb(152, 150, 152)';
    this.palettes[0x11] = 'rgb(8, 76, 196)';
    this.palettes[0x12] = 'rgb(48, 50, 236)';
    this.palettes[0x13] = 'rgb(92, 30, 228)';
    this.palettes[0x14] = 'rgb(136, 20, 176)';
    this.palettes[0x15] = 'rgb(160, 20, 100)';
    this.palettes[0x16] = 'rgb(152, 34, 32)';
    this.palettes[0x17] = 'rgb(120, 60, 0)';
    this.palettes[0x18] = 'rgb(84, 90, 0)';
    this.palettes[0x19] = 'rgb(40, 114, 0)';
    this.palettes[0x1A] = 'rgb(8, 124, 0)';
    this.palettes[0x1B] = 'rgb(0, 118, 40)';
    this.palettes[0x1C] = 'rgb(0, 102, 120)';
    this.palettes[0x1D] = 'rgb(0, 0, 0)';
    this.palettes[0x1E] = 'rgb(0, 0, 0)';
    this.palettes[0x1F] = 'rgb(0, 0, 0)';

    this.palettes[0x20] = 'rgb(236, 238, 236)';
    this.palettes[0x21] = 'rgb(76, 154, 236)';
    this.palettes[0x22] = 'rgb(120, 124, 236)';
    this.palettes[0x23] = 'rgb(176, 98, 236)';
    this.palettes[0x24] = 'rgb(228, 84, 236)';
    this.palettes[0x25] = 'rgb(236, 88, 180)';
    this.palettes[0x26] = 'rgb(236, 106, 100)';
    this.palettes[0x27] = 'rgb(212, 136, 32)';
    this.palettes[0x28] = 'rgb(160, 170, 0)';
    this.palettes[0x29] = 'rgb(116, 196, 0)';
    this.palettes[0x2A] = 'rgb(76, 208, 32)';
    this.palettes[0x2B] = 'rgb(56, 204, 108)';
    this.palettes[0x2C] = 'rgb(56, 180, 204)';
    this.palettes[0x2D] = 'rgb(60, 60, 60)';
    this.palettes[0x2E] = 'rgb(0, 0, 0)';
    this.palettes[0x2F] = 'rgb(0, 0, 0)';

    this.palettes[0x30] = 'rgb(236, 238, 236)';
    this.palettes[0x31] = 'rgb(168, 204, 236)';
    this.palettes[0x32] = 'rgb(188, 188, 236)';
    this.palettes[0x33] = 'rgb(212, 178, 236)';
    this.palettes[0x34] = 'rgb(236, 174, 236)';
    this.palettes[0x35] = 'rgb(236, 174, 212)';
    this.palettes[0x36] = 'rgb(236, 180, 176)';
    this.palettes[0x37] = 'rgb(228, 196, 144)';
    this.palettes[0x38] = 'rgb(204, 210, 120)';
    this.palettes[0x39] = 'rgb(180, 222, 120)';
    this.palettes[0x3A] = 'rgb(168, 226, 144)';
    this.palettes[0x3B] = 'rgb(152, 226, 180)';
    this.palettes[0x3C] = 'rgb(160, 214, 228)';
    this.palettes[0x3D] = 'rgb(160, 162, 160)';
    this.palettes[0x3E] = 'rgb(0, 0, 0)';
    this.palettes[0x3F] = 'rgb(0, 0, 0)';

    // Set all frame pixels black
    for (let i = 0; i < this.width; i += 1) {
      this.frame[i] = new Uint8Array(this.height);
      for (let j = 0; j < this.height; j += 1) {
        this.frame[i][j] = 0x3F;
      }
    }

    // Initialize Loopy Registers
    this.vRamAddress = {
      coarseX: 0b00000,
      coarseY: 0b00000,
      nametableX: 0b0,
      nametableY: 0b0,
      fineY: 0b111,
      unused: 0b0,
      register: 0x0000,
    };

    this.tRamAddress = {
      coarseX: 0b00000,
      coarseY: 0b00000,
      nametableX: 0b0,
      nametableY: 0b0,
      fineY: 0b111,
      unused: 0b0,
      register: 0x0000,
    };
  }

  connectCartridge(c: Cartridge): void {
    this.cartridge = c;
  }

  incrementScrollX(): void {
    if (this.maskRegister & MaskPPU.renderBackground & MaskPPU.renderSprites) {
      if (this.vRamAddress.coarseX === 31) {
        this.vRamAddress.coarseX = 0;
      } else {
        this.vRamAddress.coarseX += 1;
      }
    }
  }

  incrementScrollY(): void {
    if (this.maskRegister & MaskPPU.renderBackground & MaskPPU.renderSprites) {
      if (this.vRamAddress.fineY < 7) {
        this.vRamAddress.fineY += 1;
      } else {
        this.vRamAddress.fineY = 0;
        if (this.vRamAddress.coarseY === 29) {
          this.vRamAddress.coarseY = 0;
          this.vRamAddress.nametableY = (~this.vRamAddress.nametableY & 0b1) as uint8;
        } else if (this.vRamAddress.coarseY === 29) {
          this.vRamAddress.coarseY = 0;
        } else {
          this.vRamAddress.coarseY += 1;
        }
      }
    }
  }

  transferAddressX(): void {
    if (this.maskRegister & MaskPPU.renderBackground & MaskPPU.renderSprites) {
      this.vRamAddress.nametableX = this.tRamAddress.nametableX;
      this.vRamAddress.coarseX = this.tRamAddress.coarseX;
    }
  }

  transferAddressY(): void {
    if (this.maskRegister & MaskPPU.renderBackground & MaskPPU.renderSprites) {
      this.vRamAddress.fineY = this.tRamAddress.fineY;
      this.vRamAddress.nametableY = this.tRamAddress.nametableY;
      this.vRamAddress.coarseY = this.tRamAddress.coarseY;
    }
  }

  loadBackgroundShifters(): void {
    this.bgShifterPatternLo = (this.bgShifterPatternLo & 0xFF00) | this.bgNextTileLsb;
    this.bgShifterPatternHi = (this.bgShifterPatternHi & 0xFF00) | this.bgNextTileMsb;
    this.bgShifterAttributeLo = (this.bgShifterAttributeLo & 0xFF00) | ((this.bgNextTileAttribute & 0b01) ? 0xFF : 0x00);
    this.bgShifterAttributeHi = (this.bgShifterAttributeHi & 0xFF00) | ((this.bgNextTileAttribute & 0b10) ? 0xFF : 0x00);
  }

  updateShifters(): void {
    if (this.maskRegister & MaskPPU.renderBackground) {
      this.bgShifterPatternLo <<= 1;
      this.bgShifterPatternHi <<= 1;
      this.bgShifterAttributeLo <<= 1;
      this.bgShifterAttributeHi <<= 1;
    }
  }

  clock(): void {
    if (this.scanline >= -1 && this.scanline < 240) {
      if (this.scanline === -1 && this.cycle === 1) {
        this.statusRegister &= ~StatusPPU.verticalBlank; // Unset vertical blank flag
      }

      // Every visible cycle
      if ((this.cycle >= 2 && this.cycle < 258) || (this.cycle >= 321 && this.cycle < 338)) {
        this.updateShifters();
        switch ((this.cycle - 1) % 8) {
          case 0:
            this.loadBackgroundShifters();
            this.bgNextTileId = this.readBusPPU(0x2000 | (this.vRamAddress.register & 0x0FFF));
            break;
          case 2:
            this.bgNextTileAttribute = this.readBusPPU(0x23C0
              | (this.vRamAddress.nametableY << 11)
              | (this.vRamAddress.nametableX << 10)
              | ((this.vRamAddress.coarseY >> 2) << 3)
              | (this.vRamAddress.coarseX >> 2));
            if (this.vRamAddress.coarseY & 0x02) this.bgNextTileAttribute >>= 4;
            if (this.vRamAddress.coarseX & 0x02) this.bgNextTileAttribute >>= 2;
            this.bgNextTileAttribute &= 0x03;
            break;
          case 4:
            this.bgNextTileLsb = this.readBusPPU(
              (this.controlRegister & ControlPPU.patternBackground) > 0 ? 1 << 12 : 0
                + this.bgNextTileId << 4
                + this.vRamAddress.fineY + 0,
            );
            break;
          case 6:
            this.bgNextTileMsb = this.readBusPPU(
              (this.controlRegister & ControlPPU.patternBackground) > 0 ? 1 << 12 : 0
                + this.bgNextTileId << 4
                + this.vRamAddress.fineY + 8,
            );
            break;
          case 7:
            this.incrementScrollX();
            break;
          default:
            break;
        }
      }

      if (this.cycle === 256) {
        this.incrementScrollY();
      }

      if (this.cycle === 257) {
        this.transferAddressX();
      }

      if (this.scanline === -1 && this.cycle >= 280 && this.cycle < 305) {
        this.transferAddressY();
      }
    }

    if (this.scanline === 240) {
      // Nothing happens
    }


    if (this.scanline === 241 && this.cycle === 1) {
      this.statusRegister |= StatusPPU.verticalBlank; // Set vertical blank flag
      if ((this.controlRegister & ControlPPU.enableNmi) > 0) {
        this.nmi = true;
      }
    }

    let bgPixel: uint8 = 0x00;
    let bgPalette: uint8 = 0x00;

    if (this.maskRegister & MaskPPU.renderBackground) {
      const bitMux = 0x8000 >> this.fineX;

      const p0Pixel: uint8 = (this.bgShifterPatternLo & bitMux) > 0 ? 1 : 0;
      const p1Pixel: uint8 = (this.bgShifterPatternHi & bitMux) > 0 ? 1 : 0;
      bgPixel = ((p1Pixel << 1) | p0Pixel) as uint8;

      const bgPal0: uint8 = (this.bgShifterAttributeLo & bitMux) > 0 ? 1 : 0;
      const bgPal1: uint8 = (this.bgShifterAttributeHi & bitMux) > 0 ? 1 : 0;
      bgPalette = ((bgPal1 << 1) | bgPal0) as uint8;
    }

    // Set the pixel!
    if (this.cycle - 1 >= 0 && this.cycle - 1 < this.width
      && this.scanline >= 0 && this.scanline < this.height) {
      this.frame[this.cycle - 1][this.scanline] = this.getColorFromPaletteRam(bgPalette, bgPixel);
    }


    this.cycle += 1;

    if (this.cycle >= 341) {
      this.cycle = 0;
      this.scanline += 1;
      this.lineComplete = true;

      if (this.scanline >= 261) {
        this.scanline = -1;
        this.frameComplete = true;
      }
    }
  }

  getScreen(): Array<Uint8Array> {
    return this.frame;
  }

  getNameTable(i: uint8): Uint8Array {
    return this.sprNameTable[i];
  }

  getPatternTable(i: uint8, selectedPalette: uint8): Uint8Array {
    for (let nTileY = 0; nTileY < 16; nTileY += 1) {
      for (let nTileX = 0; nTileX < 16; nTileX += 1) {
        const nOffset = nTileY * 256 + nTileX * 16;

        for (let row = 0; row < 8; row += 1) {
          let tileLSB = this.readBusPPU(i * 0x1000 + nOffset + row + 0);
          let tileMSB = this.readBusPPU(i * 0x1000 + nOffset + row + 8);

          for (let column = 0; column < 8; column += 1) {
            const pixel: uint8 = ((tileMSB & 0x01) + (tileLSB & 0x01)) as uint8;
            tileLSB >>= 1;
            tileMSB >>= 1;
            const x = nTileX * 8 + (7 - column);
            const y = nTileY * 8 + row;
            this.sprPatternTable[i][x * 128 + y] = this.getColorFromPaletteRam(selectedPalette, pixel);
          }
        }
      }
    }
    return this.sprPatternTable[i];
  }

  getColorFromPaletteRam(palette: uint8, pixel: uint8): uint8 {
    return this.readBusPPU(0x3F00 + (palette << 2) + pixel);
  }

  readBusCPU(address: uint16, readOnly?: boolean): uint8 {
    let data: uint8 = 0x00;
    switch (address) {
      case 0x0000: // Control
        break;
      case 0x0001: // Mask
        break;
      case 0x0002: // Status
        /*
          When reading the status register, we are only interested on the
          first 3 bits. The rest of them are filled with noise or more likely
          the content of the buffered data.
        */
        data = ((this.statusRegister & 0xE0) | (this.ppuDataBuffer & 0x1F)) as uint8;
        this.statusRegister &= ~StatusPPU.verticalBlank; // Clear vertical blank bit
        this.addressLatch = 0;
        break;
      case 0x0003: // OAM Address
        break;
      case 0x0004: // OAM Data
        break;
      case 0x0005: // Scroll
        break;
      case 0x0006: // PPU Address
        break;
      case 0x0007: // PPU Data
        /*
          Regular reads are delayed by one read as they require many internal cycles
          to be performed. However, reading to the palette table happens in the same
          cycle.
        */
        data = this.ppuDataBuffer;
        this.ppuDataBuffer = this.readBusPPU(this.vRamAddress.register);

        if (this.vRamAddress.register >= 0x3F00) data = this.ppuDataBuffer;
        this.vRamAddress.register += (this.controlRegister & ControlPPU.incrementMode) > 0 ? 32 : 1;
        break;
      default:
        break;
    }
    return data;
  }
  writeBusCPU(address: uint16, data: uint8): void {
    switch (address) {
      case 0x0000: // Control
        this.controlRegister = data;
        this.tRamAddress.nametableX = (this.controlRegister & ControlPPU.nametableX) > 0 ? 1 : 0;
        this.tRamAddress.nametableY = (this.controlRegister & ControlPPU.nametableY) > 0 ? 1 : 0;
        break;
      case 0x0001: // Mask
        this.maskRegister = data;
        break;
      case 0x0002: // Status
        break;
      case 0x0003: // OAM Address
        break;
      case 0x0004: // OAM Data
        break;
      case 0x0005: // Scroll
        if (this.addressLatch === 0) {
          this.fineX = (data & 0x07) as uint8;
          this.tRamAddress.coarseX = data >> 3;
          this.addressLatch = 1;
        } else {
          this.tRamAddress.fineY = (data & 0x07) as uint8;
          this.tRamAddress.coarseY = data >> 3;
          this.addressLatch = 0;
        }
        break;
      case 0x0006: // PPU Address
        if (this.addressLatch === 0) {
          this.tRamAddress.register = (this.tRamAddress.register & 0x00FF) | (data << 8);
          this.addressLatch = 1;
        } else {
          this.tRamAddress.register = (this.tRamAddress.register & 0xFF00) | data;
          this.vRamAddress = { ...this.tRamAddress };
          this.addressLatch = 0;
        }
        break;
      case 0x0007: // PPU Data
        this.writeBusPPU(this.tRamAddress.register, data);
        this.tRamAddress.register += (this.controlRegister & ControlPPU.incrementMode) > 0 ? 32 : 1;
        break;
      default:
        break;
    }
  }
  readBusPPU(address: uint16, readOnly?: boolean): uint8 {
    const data: Pointer = { value: 0x00 };
    let mappedAddress = address & 0x3FFF;

    if (this.cartridge.readBusPPU(mappedAddress, data)) {
      // Read handled by cartridge
    } else if (mappedAddress >= 0x0000 && mappedAddress <= 0x1FFF) {
      // Pattern Memory
      data.value = this.tblPattern[(mappedAddress & 0x1000) >> 12][mappedAddress & 0x0FFF];
    } else if (mappedAddress >= 0x2000 && mappedAddress <= 0x3EFF) {
      // Name Table
      mappedAddress &= 0x0FFF;
      if (this.cartridge.mirroring === Mirroring.vertical) {
        // Vertical
        if (mappedAddress >= 0x0000 && mappedAddress <= 0x03FF) {
          data.value = this.tblName[0][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0400 && mappedAddress <= 0x07FF) {
          data.value = this.tblName[1][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0800 && mappedAddress <= 0x0BFF) {
          data.value = this.tblName[0][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0C00 && mappedAddress <= 0x0FFF) {
          data.value = this.tblName[1][mappedAddress & 0x03FF];
        }
      } else if (this.cartridge.mirroring === Mirroring.horizontal) {
        // Horizontal
        if (mappedAddress >= 0x0000 && mappedAddress <= 0x03FF) {
          data.value = this.tblName[0][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0400 && mappedAddress <= 0x07FF) {
          data.value = this.tblName[0][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0800 && mappedAddress <= 0x0BFF) {
          data.value = this.tblName[1][mappedAddress & 0x03FF];
        } else if (mappedAddress >= 0x0C00 && mappedAddress <= 0x0FFF) {
          data.value = this.tblName[1][mappedAddress & 0x03FF];
        }
      }
    } else if (mappedAddress >= 0x3F00 && mappedAddress <= 0x3FFF) {
      // Palettes Memory
      let target = mappedAddress & 0x001F;

      // Mirroring
      if (mappedAddress === 0x0010) target = 0x0000;
      if (mappedAddress === 0x0014) target = 0x0004;
      if (mappedAddress === 0x0018) target = 0x0008;
      if (mappedAddress === 0x001c) target = 0x000C;

      data.value = this.tblPalette[target];
    }

    return (data.value & 0xFF) as uint8;
  }
  writeBusPPU(address: uint16, data: uint8): void {
    let mappedAddress = address & 0x3FFF;

    if (this.cartridge.writeBusPPU(mappedAddress, data)) {
      // Write handled by cartridge
    } else if (mappedAddress >= 0x0000 && mappedAddress <= 0x1FFF) {
      // Pattern Memory (ROM, but write just in case. In some carts its RAM.)
      this.tblPattern[(mappedAddress & 0x1000) >> 12][mappedAddress & 0x0FFF] = data;
    } else if (mappedAddress >= 0x2000 && mappedAddress <= 0x3EFF) {
      // Name Table
      mappedAddress &= 0x0FFF;
      if (this.cartridge.mirroring === Mirroring.vertical) {
        // Vertical
        if (mappedAddress >= 0x0000 && mappedAddress <= 0x03FF) {
          this.tblName[0][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0400 && mappedAddress <= 0x07FF) {
          this.tblName[1][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0800 && mappedAddress <= 0x0BFF) {
          this.tblName[0][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0C00 && mappedAddress <= 0x0FFF) {
          this.tblName[1][mappedAddress & 0x03FF] = data;
        }
      } else if (this.cartridge.mirroring === Mirroring.horizontal) {
        // Horizontal
        if (mappedAddress >= 0x0000 && mappedAddress <= 0x03FF) {
          this.tblName[0][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0400 && mappedAddress <= 0x07FF) {
          this.tblName[0][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0800 && mappedAddress <= 0x0BFF) {
          this.tblName[1][mappedAddress & 0x03FF] = data;
        } else if (mappedAddress >= 0x0C00 && mappedAddress <= 0x0FFF) {
          this.tblName[1][mappedAddress & 0x03FF] = data;
        }
      }
    } else if (mappedAddress >= 0x3F00 && mappedAddress <= 0x3FFF) {
      // Palettes Memory
      let target = mappedAddress & 0x001F;

      // Mirroring
      if (mappedAddress === 0x0010) target = 0x0000;
      if (mappedAddress === 0x0014) target = 0x0004;
      if (mappedAddress === 0x0018) target = 0x0008;
      if (mappedAddress === 0x001c) target = 0x000C;

      this.tblPalette[target] = data;
    }
  }
}

export default Visual2C02;
