import Mapper from './Mapper';
import { Pointer } from './interfaces';
import { uint16 } from './types';

class Mapper000 extends Mapper {
  cpuMapRead(addr: uint16, mappedAddr: Pointer): boolean {
    if (addr >= 0x8000 && addr <= 0xFFFF) {
      mappedAddr.value = addr & (this.prgBanks > 1 ? 0x7FFF : 0x3FFF);
      return true;
    }
    return false;
  }

  cpuMapWrite(addr: uint16, mappedAddr: Pointer): boolean {
    if (addr >= 0x8000 && addr <= 0xFFFF) {
      return true;
    }
    return false;
  }

  ppuMapRead(addr: uint16, mappedAddr: Pointer): boolean {
    if (addr >= 0x0000 && addr <= 0x1FFF) {
      mappedAddr.value = addr;
      return true;
    }
    return false;
  }

  ppuMapWrite(addr: uint16, mappedAddr: Pointer): boolean {
    return false;
  }
}

export default Mapper000;
