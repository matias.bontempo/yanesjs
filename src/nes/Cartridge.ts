import { uint8, uint16 } from './types';
import Mapper from './Mapper';
import Mapper000 from './Mapper000';
import { Pointer } from './interfaces';
import { Mirroring } from './enums';

class Cartridge {
  vPRGMemory: Uint8Array;
  vCHRMemory: Uint8Array;

  nMapperID: uint8 = 0;
  nPRGBanks: uint8 = 0;
  nCHRBanks: uint8 = 0;

  mirroring: Mirroring;

  pMapper: Mapper;

  constructor(buffer: Buffer) {
    const name = buffer.readUInt32BE(0);
    this.nCHRBanks = buffer.readUInt8(4) as uint8;
    this.nPRGBanks = buffer.readUInt8(5) as uint8;
    const flags6 = buffer.readUInt8(6);
    const flags7 = buffer.readUInt8(7);
    // const prgRamSize = buffer.readUInt8(8);
    // const tvSystem1 = buffer.readUInt8(9);
    // const tvSystem2 = buffer.readUInt8(10);
    // const unused = buffer.readUInt8(5);

    if (name !== 0x4E45531A) {
      throw new Error('Invalid .nes file');
    }

    const hasTrainer = (flags6 & 0b00000100) > 0;
    this.nMapperID = <uint8>(((flags7 >> 4) << 4) | (flags6 >> 4));
    this.mirroring = <Mirroring>flags6 & 0x01;

    // const fileType = 1;

    // This is only for fileType === 1
    const PRGStart = 16 + (hasTrainer ? 512 : 0);
    const PRGEnd = PRGStart + this.nPRGBanks * 0x0400 * 16; // 16k per PRG bank
    this.vPRGMemory = buffer.slice(PRGStart, PRGEnd);

    const CHRStart = PRGEnd;
    const CHREnd = CHRStart + this.nCHRBanks * 0x0400 * 8; // 8k per PRG bank
    this.vCHRMemory = buffer.slice(CHRStart, CHREnd);

    switch (this.nMapperID) {
      case 0:
        this.pMapper = new Mapper000(this.nPRGBanks, this.nCHRBanks);
        break;
      default:
        throw new Error(`Unkown mapper ${this.nMapperID}`);
    }
  }

  readBusCPU(address: uint16, data: Pointer): boolean {
    const mappedAddr: Pointer = { value: 0 };
    if (this.pMapper.cpuMapRead(address, mappedAddr)) {
      data.value = this.vPRGMemory[mappedAddr.value];
      return true;
    }
    return false;
  }

  writeBusCPU(address: uint16, data: uint8): boolean {
    const mappedAddr: Pointer = { value: 0 };
    if (this.pMapper.cpuMapRead(address, mappedAddr)) {
      this.vPRGMemory[mappedAddr.value] = data;
      return true;
    }
    return false;
  }

  readBusPPU(address: uint16, data: Pointer): boolean {
    const mappedAddr: Pointer = { value: 0 };
    if (this.pMapper.ppuMapRead(address, mappedAddr)) {
      data.value = this.vCHRMemory[mappedAddr.value];
      return true;
    }
    return false;
  }

  writeBusPPU(address: uint16, data: uint8): boolean {
    const mappedAddr: Pointer = { value: 0 };
    if (this.pMapper.ppuMapWrite(address, mappedAddr)) {
      this.vPRGMemory[mappedAddr.value] = data;
      return true;
    }
    return false;
  }
}

export default Cartridge;
