import Bus from './NES';

import type { uint8, uint16 } from './types';
import { Flags } from './enums';
import { Instruction, DisassembledInstruction } from './interfaces';

import nestestDebug from './utils/nestestDebug';

class MOS6502 {
  // Attributes Definition

  a: uint8 = 0x00;
  x: uint8 = 0x00;
  y: uint8 = 0x00;

  status: uint8 = 0b00000000;

  stackPointer: uint8 = 0x00;
  programCounter: uint16 = 0x0000;

  fetched: uint8 = 0x00;
  addrAbs: uint16 = 0x0000;
  addrRel: uint16 = 0x0000;

  opCode: uint8 = 0x00;

  cycles: uint8 = 0x00;

  bus: Bus;

  testingEnabled: boolean = false;
  elapsedCycles: number = 0;
  elapsedInstructions: number = 0;

  // Reference at: https://www.masswerk.at/6502/6502_instruction_set.html
  lookup: Instruction[] = [
    {
      name: 'BRK', operate: this.BRK, addrmode: this.IMP, cycles: 7,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'ASL', operate: this.ASL, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'PHP', operate: this.PHP, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'ASL', operate: this.ASL, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'ASL', operate: this.ASL, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BPL', operate: this.BPL, addrmode: this.REL, cycles: 2,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'ASL', operate: this.ASL, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'CLC', operate: this.CLC, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.ABY, cycles: 4,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ORA', operate: this.ORA, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'ASL', operate: this.ASL, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
    {
      name: 'JSR', operate: this.JSR, addrmode: this.ABS, cycles: 6,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: 'BIT', operate: this.BIT, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'ROL', operate: this.ROL, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'PLP', operate: this.PLP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'ROL', operate: this.ROL, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'BIT', operate: this.BIT, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'ROL', operate: this.ROL, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BMI', operate: this.BMI, addrmode: this.REL, cycles: 2,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'ROL', operate: this.ROL, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'SEC', operate: this.SEC, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.ABY, cycles: 4,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'AND', operate: this.AND, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'ROL', operate: this.ROL, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
    {
      name: 'RTI', operate: this.RTI, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'LSR', operate: this.LSR, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'PHA', operate: this.PHA, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'LSR', operate: this.LSR, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'JMP', operate: this.JMP, addrmode: this.ABS, cycles: 3,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'LSR', operate: this.LSR, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BVC', operate: this.BVC, addrmode: this.REL, cycles: 2,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'LSR', operate: this.LSR, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'CLI', operate: this.CLI, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.ABY, cycles: 4,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'EOR', operate: this.EOR, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'LSR', operate: this.LSR, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
    {
      name: 'RTS', operate: this.RTS, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'ROR', operate: this.ROR, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'PLA', operate: this.PLA, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'ROR', operate: this.ROR, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'JMP', operate: this.JMP, addrmode: this.IND, cycles: 5,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'ROR', operate: this.ROR, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BVS', operate: this.BVS, addrmode: this.REL, cycles: 2,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'ROR', operate: this.ROR, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'SEI', operate: this.SEI, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.ABY, cycles: 4,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'ADC', operate: this.ADC, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'ROR', operate: this.ROR, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
    {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'STY', operate: this.STY, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'STX', operate: this.STX, addrmode: this.ZP0, cycles: 3,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'DEY', operate: this.DEY, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'TXA', operate: this.TXA, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'STY', operate: this.STY, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'STX', operate: this.STX, addrmode: this.ABS, cycles: 4,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    },
    {
      name: 'BCC', operate: this.BCC, addrmode: this.REL, cycles: 2,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.IZY, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'STY', operate: this.STY, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'STX', operate: this.STX, addrmode: this.ZPY, cycles: 4,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'TYA', operate: this.TYA, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.ABY, cycles: 5,
    }, {
      name: 'TXS', operate: this.TXS, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'STA', operate: this.STA, addrmode: this.ABX, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    },
    {
      name: 'LDY', operate: this.LDY, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.IZX, cycles: 6,
    }, {
      name: 'LDX', operate: this.LDX, addrmode: this.IMM, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'LDY', operate: this.LDY, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'LDX', operate: this.LDX, addrmode: this.ZP0, cycles: 3,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 3,
    }, {
      name: 'TAY', operate: this.TAY, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'TAX', operate: this.TAX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'LDY', operate: this.LDY, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'LDX', operate: this.LDX, addrmode: this.ABS, cycles: 4,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    },
    {
      name: 'BCS', operate: this.BCS, addrmode: this.REL, cycles: 2,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'LDY', operate: this.LDY, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'LDX', operate: this.LDX, addrmode: this.ZPY, cycles: 4,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'CLV', operate: this.CLV, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.ABY, cycles: 4,
    }, {
      name: 'TSX', operate: this.TSX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'LDY', operate: this.LDY, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'LDA', operate: this.LDA, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'LDX', operate: this.LDX, addrmode: this.ABY, cycles: 4,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 4,
    },
    {
      name: 'CPY', operate: this.CPY, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: 'CPY', operate: this.CPY, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'DEC', operate: this.DEC, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'INY', operate: this.INY, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'DEX', operate: this.DEX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'CPY', operate: this.CPY, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'DEC', operate: this.DEC, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BNE', operate: this.BNE, addrmode: this.REL, cycles: 2,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'DEC', operate: this.DEC, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'CLD', operate: this.CLD, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.ABY, cycles: 4,
    }, {
      name: 'NOP', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'CMP', operate: this.CMP, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'DEC', operate: this.DEC, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
    {
      name: 'CPX', operate: this.CPX, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.IZX, cycles: 6,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: 'CPX', operate: this.CPX, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.ZP0, cycles: 3,
    }, {
      name: 'INC', operate: this.INC, addrmode: this.ZP0, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 5,
    }, {
      name: 'INX', operate: this.INX, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.IMM, cycles: 2,
    }, {
      name: 'NOP', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.SBC, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'CPX', operate: this.CPX, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.ABS, cycles: 4,
    }, {
      name: 'INC', operate: this.INC, addrmode: this.ABS, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    },
    {
      name: 'BEQ', operate: this.BEQ, addrmode: this.REL, cycles: 2,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.IZY, cycles: 5,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 8,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.ZPX, cycles: 4,
    }, {
      name: 'INC', operate: this.INC, addrmode: this.ZPX, cycles: 6,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 6,
    }, {
      name: 'SED', operate: this.SED, addrmode: this.IMP, cycles: 2,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.ABY, cycles: 4,
    }, {
      name: 'NOP', operate: this.NOP, addrmode: this.IMP, cycles: 2,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    }, {
      name: '???', operate: this.NOP, addrmode: this.IMP, cycles: 4,
    }, {
      name: 'SBC', operate: this.SBC, addrmode: this.ABX, cycles: 4,
    }, {
      name: 'INC', operate: this.INC, addrmode: this.ABX, cycles: 7,
    }, {
      name: '???', operate: this.XXX, addrmode: this.IMP, cycles: 7,
    },
  ];

  // Flags Operations

  setFlag(flag: Flags, value: boolean): void {
    if (value) this.status |= flag;
    else this.status &= ~flag;
  }

  getFlag(flag: Flags): uint8 {
    return ((this.status & flag) > 0) ? 1 : 0;
  }


  // Memory Access

  write(addr: uint16, data: uint8): void {
    this.bus.writeBusCPU(addr, data);
  }

  read(addr: uint16): uint8 {
    return this.bus.readBusCPU(addr, false);
  }


  // Addressing Modes

  IMP(): boolean {
    this.fetched = this.a;
    return false;
  }

  IMM(): boolean {
    this.addrAbs = this.programCounter;
    this.programCounter += 1;
    return false;
  }

  ZP0(): boolean {
    this.addrAbs = this.read(this.programCounter) & 0x00FF;
    this.programCounter += 1;
    return false;
  }

  ZPX(): boolean {
    this.addrAbs = (this.read(this.programCounter) + this.x) & 0x00FF;
    this.programCounter += 1;
    return false;
  }

  ZPY(): boolean {
    this.addrAbs = (this.read(this.programCounter) + this.y) & 0x00FF;
    this.programCounter += 1;
    return false;
  }

  REL(): boolean {
    this.addrRel = this.read(this.programCounter);
    this.programCounter += 1;
    if (this.addrRel & 0x80) this.addrRel |= 0xFF00;
    return false;
  }

  ABS(): boolean {
    const loAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;
    const hiAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    this.addrAbs = (hiAddr << 8) | loAddr;
    return false;
  }

  ABX(): boolean {
    const loAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;
    const hiAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    this.addrAbs = (hiAddr << 8) | loAddr;
    this.addrAbs += this.x;

    if ((this.addrAbs & 0xFF00) !== (hiAddr << 8)) return true;
    return false;
  }

  ABY(): boolean {
    const loAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;
    const hiAddr: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    this.addrAbs = (hiAddr << 8) | loAddr;
    this.addrAbs += this.y;

    if ((this.addrAbs & 0xFF00) !== (hiAddr << 8)) return true;
    return false;
  }

  IND(): boolean {
    const lowPointer: uint16 = this.read(this.programCounter);
    this.programCounter += 1;
    const hiPointer: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    const pointer = (hiPointer << 8) | lowPointer;

    if (lowPointer === 0x00FF) {
      this.addrAbs = ((this.read(pointer & 0xFF00) << 8) | this.read(pointer + 0));
    } else {
      this.addrAbs = ((this.read(pointer + 1) << 8) | this.read(pointer + 0));
    }

    return false;
  }

  IZX(): boolean {
    const baseTargetAddress: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    const loAddr = this.read((baseTargetAddress + this.x) & 0x00FF);
    const hiAddr = this.read((baseTargetAddress + this.x + 1) & 0x00FF);

    this.addrAbs = (hiAddr << 8) | loAddr;

    return false;
  }

  IZY(): boolean {
    const baseTargetAddress: uint16 = this.read(this.programCounter);
    this.programCounter += 1;

    const loAddr = this.read(baseTargetAddress & 0x00FF);
    const hiAddr = this.read((baseTargetAddress + 1) & 0x00FF);

    this.addrAbs = (hiAddr << 8) | loAddr;
    this.addrAbs += this.y;

    if ((this.addrAbs & 0xFF00) !== hiAddr << 8) return true;
    return false;
  }


  // Operation Codes


  // Add value to `a` register
  ADC(): boolean {
    this.fetch();

    const a: uint16 = this.a as uint16;
    const fetched: uint16 = this.fetched as uint16;
    const cFlag: uint16 = this.getFlag(Flags.C) as uint16;

    const result: uint16 = a + fetched + cFlag;

    this.setFlag(Flags.C, result > 0xFF);
    this.setFlag(Flags.Z, (result & 0x00FF) === 0);
    this.setFlag(Flags.N, (result & 0b10000000) > 0);
    this.setFlag(Flags.V, ((~(a ^ fetched) & (a ^ result)) & 0b10000000) > 0);

    this.a = (result & 0x00FF) as uint8;

    return true;
  }

  // AND Memory with A Register
  AND(): boolean {
    this.fetch();

    this.a = ((this.a & this.fetched) & 0xFF) as uint8;

    this.setFlag(Flags.Z, (this.a & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.a & 0b10000000) > 0);

    return true;
  }

  // Shift left one bit and saves it to A Register or Memory
  ASL(): boolean {
    this.fetch();
    const tmp: uint16 = this.fetched << 1;

    this.setFlag(Flags.C, (tmp & 0xFF00) > 0);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    if (this.lookup[this.opCode].addrmode === this.IMP) {
      this.a = (tmp & 0x00FF) as uint8;
    } else {
      this.write(this.addrAbs, (tmp & 0x00FF) as uint8);
    }

    return false;
  }

  // Branch if Carry bit is set
  BCS(): boolean {
    if (this.getFlag(Flags.C) === 1) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if Carry bit is NOT set
  BCC(): boolean {
    if (this.getFlag(Flags.C) === 0) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if Equal
  BEQ(): boolean {
    if (this.getFlag(Flags.Z) === 1) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if NOT Equal
  BNE(): boolean {
    if (this.getFlag(Flags.Z) === 0) {
      this.cycles += 1;
      this.addrAbs = (this.programCounter + this.addrRel) & 0xFFFF;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if Negative
  BMI(): boolean {
    if (this.getFlag(Flags.N) === 1) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if NOT Negative (Positive)
  BPL(): boolean {
    if (this.getFlag(Flags.N) === 0) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if Overflow
  BVS(): boolean {
    if (this.getFlag(Flags.V) === 1) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  // Branch if NOT Overflow
  BVC(): boolean {
    if (this.getFlag(Flags.V) === 0) {
      this.cycles += 1;
      this.addrAbs = this.programCounter + this.addrRel;

      if ((this.addrAbs & 0xFF00) !== (this.programCounter & 0xFF00)) {
        this.cycles += 1;
      }

      this.programCounter = this.addrAbs & 0xFFFF;
    }
    return false;
  }

  BIT(): boolean {
    this.fetch();
    const tmp = this.a & this.fetched;
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0x00);
    this.setFlag(Flags.N, (this.fetched & (1 << 7)) > 0);
    this.setFlag(Flags.V, (this.fetched & (1 << 6)) > 0);
    return false;
  }

  BRK(): boolean {
    this.programCounter += 1;

    this.setFlag(Flags.I, true);

    this.write(0x0100 + this.stackPointer, ((this.programCounter >> 8) & 0x00FF) as uint8);
    this.stackPointer -= 1;
    this.write(0x0100 + this.stackPointer, (this.programCounter & 0x00FF) as uint8);
    this.stackPointer -= 1;

    this.setFlag(Flags.B, true);
    this.write(0x0100 + this.stackPointer, this.status);
    this.stackPointer -= 1;

    this.setFlag(Flags.B, false);

    this.programCounter = this.read(0xFFFE) | (this.read(0xFFFF) << 8);

    return false;
  }

  // Clear Carry bit
  CLC(): boolean {
    this.setFlag(Flags.C, false);
    return false;
  }

  // Clear Decimal Mode bit
  CLD(): boolean {
    this.setFlag(Flags.D, false);
    return false;
  }

  // Clear Disable Interruptions bit
  CLI(): boolean {
    this.setFlag(Flags.I, false);
    return false;
  }

  // Clear Overflow bit
  CLV(): boolean {
    this.setFlag(Flags.V, false);
    return false;
  }

  // Compare Memory with A Register
  CMP(): boolean {
    this.fetch();
    const tmp = this.a - this.fetched;

    this.setFlag(Flags.C, this.a >= this.fetched);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    return true;
  }

  // Compare Memory with X Register
  CPX(): boolean {
    this.fetch();
    const tmp = this.x - this.fetched;

    this.setFlag(Flags.C, this.x >= this.fetched);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    return false;
  }

  // Compare Memory with Y Register
  CPY(): boolean {
    this.fetch();
    const tmp = this.y - this.fetched;

    this.setFlag(Flags.C, this.y >= this.fetched);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    return false;
  }

  // Decrement value at Memory
  DEC(): boolean {
    this.fetch();

    const tmp = this.fetched - 1;
    this.write(this.addrAbs, (tmp & 0x00FF) as uint8);

    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    return false;
  }

  // Decrement X Register
  DEX(): boolean {
    this.x -= 1;

    this.setFlag(Flags.Z, (this.x & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.x & 0b10000000) > 0);

    return false;
  }

  // Decrement Y Register by one
  DEY(): boolean {
    this.y -= 1;
    this.setFlag(Flags.Z, this.y === 0x00);
    this.setFlag(Flags.N, (this.y & 0x80) > 0);
    return false;
  }

  // XOR
  EOR(): boolean {
    this.fetch();

    this.a ^= this.fetched;

    this.setFlag(Flags.Z, this.a === 0x00);
    this.setFlag(Flags.N, (this.a & 0x80) > 0);

    return true;
  }

  // Increment value at Memory
  INC(): boolean {
    this.fetch();

    const tmp = this.fetched + 1;
    this.write(this.addrAbs, (tmp & 0x00FF) as uint8);

    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    return false;
  }

  // Increment X Register
  INX(): boolean {
    this.x += 1;

    this.setFlag(Flags.Z, (this.x & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.x & 0b10000000) > 0);

    return false;
  }

  // Increment Y Register
  INY(): boolean {
    this.y += 1;

    this.setFlag(Flags.Z, (this.y & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.y & 0b10000000) > 0);

    return false;
  }

  // JMP to address
  JMP(): boolean {
    this.programCounter = this.addrAbs;
    return false;
  }

  // Jump to subroutine
  JSR(): boolean {
    this.programCounter -= 1;

    this.write(0x0100 + this.stackPointer, ((this.programCounter >> 8) & 0x00FF) as uint8);
    this.stackPointer -= 1;
    this.write(0x0100 + this.stackPointer, (this.programCounter & 0x00FF) as uint8);
    this.stackPointer -= 1;

    this.programCounter = this.addrAbs;
    return false;
  }

  // Load Accumulator
  LDA(): boolean {
    this.fetch();
    this.a = this.fetched;
    this.setFlag(Flags.Z, this.a === 0x00);
    this.setFlag(Flags.N, (this.a & 0x80) > 0);
    return false;
  }

  // Load X Register with Memory
  LDX(): boolean {
    this.fetch();
    this.x = this.fetched;
    this.setFlag(Flags.Z, this.x === 0x00);
    this.setFlag(Flags.N, (this.x & 0x80) > 0);
    return true;
  }

  // Load Y Register with Memory
  LDY(): boolean {
    this.fetch();
    this.y = this.fetched;
    this.setFlag(Flags.Z, this.y === 0x00);
    this.setFlag(Flags.N, (this.y & 0x80) > 0);
    return true;
  }

  // Shift one bit right
  LSR(): boolean {
    this.fetch();

    this.setFlag(Flags.C, (this.fetched & 0x0001) > 0);

    const tmp: uint16 = this.fetched >> 1;

    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    if (this.lookup[this.opCode].addrmode === this.IMP) {
      this.a = (tmp & 0x00FF) as uint8;
    } else {
      this.write(this.addrAbs, (tmp & 0x00FF) as uint8);
    }

    return false;
  }

  // No Opearation
  NOP(): boolean {
    switch (this.opCode) {
      case 0x1C:
      case 0x3C:
      case 0x5C:
      case 0x7C:
      case 0xDC:
      case 0xFC:
        return true;
      default:
        return false;
    }
  }

  // Logical OR with A
  ORA(): boolean {
    this.fetch();
    this.a |= this.fetched;

    this.setFlag(Flags.Z, (this.a & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.a & 0b10000000) > 0);

    return true;
  }

  // Write to the Stack
  PHA(): boolean {
    this.write(0x0100 + this.stackPointer, this.a);
    this.stackPointer -= 1;
    return false;
  }

  // Push status register to Stack
  PHP(): boolean {
    this.write(0x0100 + this.stackPointer, (this.status | Flags.B | Flags.U) as uint8);
    this.stackPointer -= 1;

    this.setFlag(Flags.B, false);
    this.setFlag(Flags.U, false);

    return false;
  }

  // Read from the Stack
  PLA(): boolean {
    this.stackPointer += 1;
    this.a = this.read(0x0100 + this.stackPointer);

    this.setFlag(Flags.Z, this.a === 0x00);
    this.setFlag(Flags.N, (this.a & 0b10000000) > 0);
    return false;
  }

  // Pop status from stack
  PLP(): boolean {
    this.stackPointer += 1;
    this.status = this.read(0x0100 + this.stackPointer);
    this.setFlag(Flags.U, true);
    return false;
  }

  // Rotate one bit Left
  ROL(): boolean {
    this.fetch();

    const tmp = (this.fetched << 1) | this.getFlag(Flags.C);

    this.setFlag(Flags.C, (tmp & 0xFF00) > 0);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0x0000);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    if (this.lookup[this.opCode].addrmode === this.IMP) {
      this.a = (tmp & 0x00FF) as uint8;
    } else {
      this.write(this.addrAbs, (tmp & 0x00FF) as uint8);
    }

    return false;
  }

  // Rotate one bit Right
  ROR(): boolean {
    this.fetch();

    const tmp = (this.getFlag(Flags.C) << 7) | (this.fetched >> 1);

    this.setFlag(Flags.C, (this.fetched & 0x0001) > 0);
    this.setFlag(Flags.Z, (tmp & 0x00FF) === 0x0000);
    this.setFlag(Flags.N, (tmp & 0b10000000) > 0);

    if (this.lookup[this.opCode].addrmode === this.IMP) {
      this.a = (tmp & 0x00FF) as uint8;
    } else {
      this.write(this.addrAbs, (tmp & 0x00FF) as uint8);
    }

    return false;
  }

  // Return from Interruption
  RTI(): boolean {
    this.stackPointer += 1;
    this.status = this.read(0x0100 + this.stackPointer);

    this.status &= ~Flags.B;
    this.status &= ~Flags.U;

    this.stackPointer += 1;
    this.programCounter = this.read(0x0100 + this.stackPointer) & 0x00FF;
    this.stackPointer += 1;
    this.programCounter |= this.read(0x0100 + this.stackPointer) << 8;

    return false;
  }

  // Return from Subroutine
  RTS(): boolean {
    this.stackPointer += 1;
    this.programCounter = this.read(0x0100 + this.stackPointer);
    this.stackPointer += 1;
    this.programCounter |= this.read(0x0100 + this.stackPointer) << 8;

    this.programCounter += 1;

    return false;
  }

  // Subtract value from `a` register
  SBC(): boolean {
    this.fetch();

    const a: uint16 = this.a as uint16;
    const fetched: uint16 = (this.fetched ^ 0x00FF) as uint16;
    const cFlag: uint16 = this.getFlag(Flags.C) as uint16;

    const result: uint16 = a + fetched + cFlag;

    this.setFlag(Flags.C, result > 0xFF);
    this.setFlag(Flags.Z, (result & 0x00FF) === 0);
    this.setFlag(Flags.N, (result & 0b10000000) > 0);
    this.setFlag(Flags.V, ((~(a ^ fetched) & (a ^ result)) & 0b10000000) > 0);

    this.a = (result & 0x00FF) as uint8;

    return true;
  }

  // Set Carry flag
  SEC(): boolean {
    this.setFlag(Flags.C, true);
    return false;
  }

  // Set Decimal flag
  SED(): boolean {
    this.setFlag(Flags.D, true);
    return false;
  }

  // Set Interruptions flag
  SEI(): boolean {
    this.setFlag(Flags.I, true);
    return false;
  }

  // Store A Register at Memory location
  STA(): boolean {
    this.write(this.addrAbs, this.a);
    return false;
  }

  // Store X Register at Memory location
  STX(): boolean {
    this.write(this.addrAbs, this.x);
    return false;
  }

  // Store Y Register at Memory location
  STY(): boolean {
    this.write(this.addrAbs, this.y);
    return false;
  }

  // Transfer A to X
  TAX(): boolean {
    this.x = this.a;
    this.setFlag(Flags.Z, (this.x & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.x & 0b10000000) > 0);
    return false;
  }

  // Transfer A to Y
  TAY(): boolean {
    this.y = this.a;
    this.setFlag(Flags.Z, (this.y & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.y & 0b10000000) > 0);
    return false;
  }

  // Transfer Stack Pointer to X
  TSX(): boolean {
    this.x = this.stackPointer;
    this.setFlag(Flags.Z, (this.x & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.x & 0b10000000) > 0);
    return false;
  }

  // Transfer X to A
  TXA(): boolean {
    this.a = this.x;
    this.setFlag(Flags.Z, (this.a & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.a & 0b10000000) > 0);
    return false;
  }

  // Transfer X to Stack Pointer
  TXS(): boolean {
    this.stackPointer = this.x;
    return false;
  }

  // Transfer Y to A
  TYA(): boolean {
    this.a = this.y;
    this.setFlag(Flags.Z, (this.a & 0x00FF) === 0);
    this.setFlag(Flags.N, (this.a & 0b10000000) > 0);
    return false;
  }

  // Capture illegal operation codes
  XXX(): boolean { return false; }


  // Internal Events

  clock(): void {
    if (this.cycles === 0) {
      // Testing nestest against last excecuted instruction
      if (this.testingEnabled && this.elapsedInstructions < nestestDebug.length) {
        const previous = nestestDebug[this.elapsedInstructions - 1];
        const current = nestestDebug[this.elapsedInstructions];
        const [programCounter, a, x, y, status, stackPointer, ppuP, ppuS, cycles] = current;

        // if ((this.status & ~Flags.U) !== (status & ~Flags.U)) throw new Error(`status @ #${programCounter.toString(16).padStart(4, '0000')}: expected '${status.toString(2).padStart(8, '00000000')}' but found '${this.status.toString(2).padStart(8, '00000000')}'`);

        if (this.a !== a) throw new Error(`a @ #${previous[0].toString(16).padStart(4, '0000')}: expected '${a}' but found '${this.a}'`);
        if (this.x !== x) throw new Error(`x @ #${previous[0].toString(16).padStart(4, '0000')}: expected '${x}' but found '${this.x}'`);
        if (this.y !== y) throw new Error(`y @ #${previous[0].toString(16).padStart(4, '0000')}: expected '${y}' but found '${this.y}'`);
        if (this.stackPointer !== stackPointer) throw new Error(`stackPointer @ #${previous[0].toString(16).padStart(4, '0000')}: expected '${stackPointer}' but found '${this.stackPointer}'`);
        // if (this.elapsedCycles !== cycles) throw new Error(`cycles: expected '${cycles}' but found '${this.elapsedCycles }'`);
        if (this.programCounter !== programCounter) {
          throw new Error(`programCounter at ${nestestDebug[this.elapsedInstructions - 1][0].toString(16).padStart(4, '0000')}: Expected ${programCounter.toString(16).padStart(4, '0000')} but found ${this.programCounter.toString(16).padStart(4, '0000')}`);
        }
        this.elapsedInstructions += 1;
      }

      this.opCode = this.read(this.programCounter);
      this.programCounter += 1;

      const inst: Instruction = this.lookup[this.opCode];

      const additionalCycle1 = inst.addrmode.bind(this)();
      const additionalCycle2 = inst.operate.bind(this)();

      // Cap all registers
      this.a = (this.a & 0xFF) as uint8;
      this.x = (this.x & 0xFF) as uint8;
      this.y = (this.y & 0xFF) as uint8;
      this.addrAbs &= 0xFFFF;

      this.stackPointer = (this.stackPointer & 0xFF) as uint8;
      this.programCounter = (this.programCounter & 0xFFFF) as uint16;


      this.cycles = inst.cycles;
      if (additionalCycle1 && additionalCycle2) this.cycles += 1;
    }

    this.cycles -= 1;
    this.elapsedCycles += 1;
  }

  reset(): void {
    this.a = 0x00;
    this.x = 0x00;
    this.y = 0x00;

    this.stackPointer = 0xFD;
    this.status = (0b00000000 | Flags.U | Flags.I) as uint8;

    this.addrAbs = 0xFFFC;
    const loAddr: uint16 = this.read(this.addrAbs);
    const hiAddr: uint16 = this.read(this.addrAbs + 1);

    this.programCounter = (hiAddr << 8) | loAddr;

    this.addrAbs = 0x0000;
    this.addrRel = 0x0000;
    this.fetched = 0x00;

    this.cycles = 8;
  }

  irq(): void {
    if (this.getFlag(Flags.I) === 0) {
      this.write(0x0100 + this.stackPointer, ((this.programCounter >> 8) & 0x00FF) as uint8);
      this.stackPointer -= 1;
      this.write(0x0100 + this.stackPointer, (this.programCounter & 0x00FF) as uint8);
      this.stackPointer -= 1;

      this.setFlag(Flags.B, false);
      this.setFlag(Flags.U, true);
      this.setFlag(Flags.I, true);

      this.write(0x0100 + this.stackPointer, this.status);
      this.stackPointer -= 1;

      this.addrAbs = 0xFFFE;
      const loAddr: uint16 = this.read(this.addrAbs);
      const hiAddr: uint16 = this.read(this.addrAbs + 1);

      this.programCounter = (hiAddr << 8) | loAddr;

      this.cycles = 7;
    }
  }

  nmi(): void {
    this.write(0x0100 + this.stackPointer, ((this.programCounter >> 8) & 0x00FF) as uint8);
    this.stackPointer -= 1;
    this.write(0x0100 + this.stackPointer, (this.programCounter & 0x00FF) as uint8);
    this.stackPointer -= 1;

    this.setFlag(Flags.B, false);
    this.setFlag(Flags.U, true);
    this.setFlag(Flags.I, true);

    this.write(0x0100 + this.stackPointer, this.status);
    this.stackPointer -= 1;

    this.addrAbs = 0xFFFA;
    const loAddr: uint16 = this.read(this.addrAbs);
    const hiAddr: uint16 = this.read(this.addrAbs + 1);

    this.programCounter = (hiAddr << 8) | loAddr;

    this.cycles = 8;
  }

  fetch(): uint8 {
    if (this.lookup[this.opCode].addrmode !== this.IMP) {
      this.fetched = this.read(this.addrAbs & 0xFFFF);
    }
    return this.fetched;
  }

  disassamble(from: uint16, end: uint16): DisassembledInstruction[] {
    const asm: DisassembledInstruction[] = [];
    let address: uint16 = from;

    while (address < end) {
      const opCode: Instruction = this.lookup[this.read(address)];
      const { addrmode } = opCode;

      let bytes = 2;
      let data = `#$${this.read(address + 1).toString(16).padStart(2, '00')}`;

      if (addrmode === this.IMP) {
        bytes = 1;
        data = '{IMP}';
      }

      if (
        addrmode === this.ABS
        || addrmode === this.ABX
        || addrmode === this.ABY
        || addrmode === this.IND
      ) {
        bytes = 3;
        data = `$${((this.read(address + 2) << 8) | this.read(address + 1)).toString(16).padStart(4, '0000')}`;
      }

      asm.push({
        address,
        operation: opCode.name,
        data,
        raw: opCode,
      });

      address += bytes;
    }

    return asm;
  }
}

export default MOS6502;
