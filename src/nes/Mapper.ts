import { uint8, uint16, uint32 } from './types';
import { Pointer } from './interfaces';

class Mapper {
  prgBanks: uint8 = 0;
  chrBanks: uint8 = 0;

  constructor(prgBanks: uint8, chrBanks: uint8) {
    this.prgBanks = prgBanks;
    this.chrBanks = chrBanks;
  }

  cpuMapRead(addr: uint16, mappedAddr: Pointer): boolean {
    console.error('Unimplemented cpuMapRead in mapper', this);
    return false;
  }

  cpuMapWrite(addr: uint16, mappedAddr: Pointer): boolean {
    console.error('Unimplemented cpuMapWrite in mapper', this);
    return false;
  }

  ppuMapRead(addr: uint16, mappedAddr: Pointer): boolean {
    console.error('Unimplemented ppuMapRead in mapper', this);
    return false;
  }

  ppuMapWrite(addr: uint16, mappedAddr: Pointer): boolean {
    console.error('Unimplemented ppuMapWrite in mapper', this);
    return false;
  }
}

export default Mapper;
