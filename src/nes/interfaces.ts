import { uint8, uint16, uint32 } from './types';

export interface Instruction {
  name: string;
  operate: () => boolean;
  addrmode: () => boolean;
  cycles: uint8;
}

export interface DisassembledInstruction {
  address: uint16,
  operation: string,
  data: string,
  raw: Instruction,
}

export interface Pointer {
  value: number,
}

export interface LoopyRegister {
  coarseX: uint16; // 0b1111100000000000
  coarseY: uint16, // 0b0000011111000000
  nametableX: uint8, // 0b0000000000100000
  nametableY: uint8, // 0b0000000000010000
  fineY: uint8, // 0b0000000000001110
  unused: uint8, // 0b0000000000000001

  register: uint16,
}
