import MOS6502 from './MOS6502';
import Visual2C02 from './Visual2C02';

import type { uint8, uint16 } from './types';
import Cartridge from './Cartridge';
import { Pointer } from './interfaces';

class Bus {
  cpu: MOS6502;
  ppu: Visual2C02;

  cartridge: Cartridge;

  ramCPU: Uint8Array = new Uint8Array(2048); // This should be part of the CPU

  systemClockCounter: number = 0;

  constructor(cpu, ppu) {
    this.cpu = cpu;
    this.ppu = ppu;

    // Initialize RAM to 0
    for (let i = 0; i < this.ramCPU.length; i += 1) this.ramCPU[0] = 0;
    // Connect CPU to BUS (Weird, check later)
    this.cpu.bus = this;
  }

  insertCartridge(c: Cartridge): void {
    this.cartridge = c;
    this.ppu.connectCartridge(c);
  }

  reset(): void {
    this.cpu.reset();
    this.systemClockCounter = 0;
  }

  clock(): void {
    this.ppu.clock();
    if (this.systemClockCounter % 3 === 0) {
      this.cpu.clock();
    }

    if (this.ppu.nmi) {
      this.cpu.nmi();
      this.ppu.nmi = false;
    }

    this.systemClockCounter += 1;
  }

  readBusCPU(addr: uint16, readOnly?: boolean): uint8 {
    const data: Pointer = { value: 0x00 };

    if (this.cartridge.readBusCPU(addr, data)) {
      // Cartridge (priority to read, based on javidx9 TODO, check how to remove)
      // `data` ""pointer"" will be modified by the excecution of the function
    } else if (addr >= 0x0000 && addr <= 0x1FFF) {
      // RAM CPU
      data.value = (this.ramCPU[addr & 0x07FF] as uint8);
    } else if (addr >= 0x2000 && addr <= 0x3FFF) {
      // PPU
      data.value = this.ppu.readBusCPU(addr & 0x0007, readOnly);
    }

    return (data.value & 0xFF) as uint8;
  }

  writeBusCPU(addr: uint16, data: uint8): void {
    if (this.cartridge.writeBusCPU(addr, data)) {
      // Cartridge (priority to read, based on javidx9 TODO, check how to remove)
    } else if (addr >= 0x0000 && addr <= 0x1FFF) {
      // RAM CPU
      this.ramCPU[addr & 0x07FF] = data;
    } else if (addr >= 0x2000 && addr <= 0x3FFF) {
      // PPU
      this.ppu.writeBusCPU(addr & 0x0007, data);
    }
  }
}

export default Bus;
