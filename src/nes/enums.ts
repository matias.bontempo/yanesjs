/* eslint-disable import/prefer-default-export */

export enum Flags {
  C = 1 << 0, // Carry
  Z = 1 << 1, // Zero
  I = 1 << 2, // Disable Interrupts
  D = 1 << 3, // Decimal Mode (Unimplemented)
  B = 1 << 4, // Break
  U = 1 << 5, // Unused
  V = 1 << 6, // Overflow
  N = 1 << 7, // Negative
}

export enum StatusPPU {
  spriteOverflow = 1 << 5,
  spriteZeroHit = 1 << 6,
  verticalBlank = 1 << 7,
}

export enum MaskPPU {
  grayscale = 1 << 0,
  renderBackgroundLeft = 1 << 1,
  renderSpritesLeft = 1 << 2,
  renderBackground = 1 << 3,
  renderSprites = 1 << 4,
  enhancedRed = 1 << 5,
  enhancedGreen = 1 << 6,
  enhancedBlue = 1 << 7,
}

export enum ControlPPU {
  nametableX = 1 << 0,
  nametableY = 1 << 1,
  incrementMode = 1 << 2,
  patternSprite = 1 << 3,
  patternBackground = 1 << 4,
  spriteSize = 1 << 5,
  slaveMode = 1 << 6,
  enableNmi = 1 << 7,
}

export enum Mirroring {
  horizontal = 0,
  vertical = 1,
  oneScreenLO = 2,
  oneScreenHI = 3,
}
